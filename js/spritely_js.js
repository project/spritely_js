(function ($) {
  Drupal.behaviors.spritelyJs = Drupal.behaviors.spritelyJs || {};
  Drupal.behaviors.spritelyJs = {
    'attach' : function(context) {
      options = Drupal.settings.spritely;
      $(context).data('spritelyJs', options);
    },
  };
  
})(jQuery);