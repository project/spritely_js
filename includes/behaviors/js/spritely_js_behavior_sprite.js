(function ($) {
  
  Drupal.behaviors.spritelyJsSprite = Drupal.behaviors.spritelyJsSprite || {};
  
  Drupal.behaviors.spritelyJsSprite = {
    attach : function(context) {
      this.options = $(context).data('spritelyJs');
      $.each(this.options, 
        function(index, instance) { 
          if (instance.type == 'spritely_js_behavior_sprite') {
            Drupal.behaviors.spritelyJsSprite.render(instance);
          }
        }
      );
    },
    options : {},
    render : function (instance) {
      selector = instance.selector;
      delete instance.selector;
      delete instance.type;
      $(selector).sprite(instance);
    }
  };
  
})(jQuery);