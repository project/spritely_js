(function ($) {
  
  Drupal.behaviors.spritelyJsPan = Drupal.behaviors.spritelyJsPan || {};
  
  Drupal.behaviors.spritelyJsPan = {
    attach : function(context) {
      this.options = $(context).data('spritelyJs');
      $.each(this.options, 
        function(index, instance) {
          if (instance.type == 'spritely_js_behavior_pan') {
            Drupal.behaviors.spritelyJsPan.render(instance);
          }
        }
      );
    },
    options : {},
    render : function (instance) {
      selector = instance.selector;
      delete instance.selector;
      delete instance.type;
      $(selector).pan(instance);
    }
  };
  
})(jQuery);