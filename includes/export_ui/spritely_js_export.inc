<?php 

$plugin = array(
  'schema' => 'spritely_js_instances',
  'access' => 'administer media_derivatives presets',
  'menu' => array(
    'menu prefix' => 'admin/structure/spritely_js',
    'menu item' => 'spritely-js-config',
    'menu title' => 'Spitely JS',
    'menu description' => 'Add/Configure Instances for Spritely JS',
  ),
  'title singular' => t('Spritely instance'),
  'title plural' => t('Spritely instances'),
  'title singular proper' => t('Spritely JS instance'),
  'title plural proper' => t('Spritely JS instances'),
  'use wizard' => TRUE,
  'form info' => array(
    'add order' => array(
      'basic' => t('Basic preset settings'),
    ),
    'edit order' => array(
      'basic' => t('Basic preset settings'),
    ),
    'clone order' => array(
      'basic' => t('Basic preset settings'),
    ),
    'forms' => array(
      'basic' => array(
        'form id' => 'spritely_js_form_basic',
      ),
      'edit' => array(
        'form id' => 'spritely_js_form_basic',
      ),
    ),
  ),
);

/**
* Define the preset add/edit form. This is basic preset information
* form callback. It configures machine name, engine, ... 
*/

function spritely_js_form_basic($form, &$form_state) {
  $new = 1;
  $form['wrapper']['name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine name'),
    '#required' => TRUE,
    '#description' => t("Enter preset's machine name."),
    '#machine_name' => array(
      'exists' => '_spritely_js_machine_name_check',
    ),
    '#default_value' => isset($form_state['item']->machine_name) ? $form_state['item']->machine_name : NULL,
  );
  $form['wrapper']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title of instance'),
    '#required' => TRUE,
    '#description' => t("Enter your Spritely instance's machine name."),
    '#disabled' => !$new && $new = $form_state['form type'] != 'clone',
    '#default_value' => isset($form_state['item']->title) ? $form_state['item']->title : NULL,
  );
  
  $form['wrapper']['behavior'] = array(
    '#type' => 'radios',
    '#title' => t('Select Type of Behavior'),
    '#description' => t("Select either sprite or pan behavior for your instance."),
    '#options' => array('spritely_js_behavior_sprite' => 'sprite', 'spritely_js_behavior_pan' => 'pan'),
    '#default_value' => isset($form_state['item']->behavior) ? $form_state['item']->behavior : NULL,
  );
  
  $form['wrapper']['load_sprite_options'] = array(
    '#type' => 'submit',
    '#value' => t('Load Sample Sprite Options'),
    '#description' => t('Load default options for the sprite behavior below.'),
    '#states' => array(
      'visible' => array('input[name="behavior"]' => array('value' => 'spritely_js_behavior_sprite')),
    ),
  );
  
    $form['wrapper']['load_pan_options'] = array(
    '#type' => 'submit',
    '#value' => t('Load Sample Pan Options'),
    '#description' => t('Load default options for the pan behavior below.'),
    '#states' => array(
      'visible' => array('input[name="behavior"]' => array('value' => 'spritely_js_behavior_pan')),
    ),
  );
  
  $form['wrapper']['selector'] = array(
    '#type' => 'textarea',
    '#title' => t('Selector'),
    '#description' => t('Enter JQuery selector(s) for your instance. Include "." and "#" characters and separate selectors with a comma.'),
    '#default_value' => isset($form_state['item']->selector) ? $form_state['item']->selector : NULL,
  );
  
  
  $previous_val = unserialize($form_state['item']->options);
  $options_val = '';
  
  if (!empty($previous_val)) {
    foreach($previous_val AS $key => $value) {
      $options_val .= $key . ':' . $value . ', ';
    }
    $options_val = rtrim($options_val, ', ');
  }
  
  $options['attributes']['target'] = '_blank';
  $documentation = l('Options Documentation', '$documentation', $options);
  
  $form['wrapper']['options'] = array(
    '#type' => 'textarea',
    '#title' => t('Options'),
    '#description' => t('Enter a comma separated list of options. Read !doc.', array('!doc' => $documentation)),
    '#default_value' => $options_val,
  );
  
  $form['wrapper']['visibility'] = array(
    '#type' => 'textarea',
    '#title' => t('Visability'),
    '#description' => t('Enter paths where you would like the neccesary assets to be loaded.'),
    '#default_value' => isset($form_state['item']->visibility) ? $form_state['item']->visibility : NULL,
  );
  
  return $form;
}

function spritely_js_form_basic_submit($form, &$form_state) {
  
  $submiter = $form_state['triggering_element']['#value'];
  
  if ($submiter == t('Load Sample Sprite Options')) {
    include_once(drupal_get_path('module', 'spritely_js') . '/includes/spritely_js_default_options.inc');
    $form_state['input']['options'] = spritely_js_return_default_sprite_options();
    $form_state['rebuild'] = TRUE;
  }
  elseif ($submiter == t('Load Sample Pan Options')) {
    include_once(drupal_get_path('module', 'spritely_js') . '/includes/spritely_js_default_options.inc');
    $form_state['input']['options'] = spritely_js_return_default_pan_options();
    $form_state['rebuild'] = TRUE;
  }
  else {
    $form_state['item']->machine_name = $form_state['values']['name'];
    $form_state['item']->title = $form_state['values']['title'];
    $form_state['item']->behavior = $form_state['values']['behavior'];
    $form_state['item']->selector = $form_state['values']['selector'];
    $form_state['item']->options = spritely_js_format_options($form_state['values']['options']);
    $form_state['item']->visibility = $form_state['values']['visibility'];
    cache_clear_all('spritely_js_instances', 'cache');
  }
}

function _spritely_js_machine_name_check($name) {
  //$presets = media_derivatives_get_presets();
  //return isset($presets[$name]);
  return false;
}

function spritely_js_format_options($options) {
  $parsed_options = array();
  $option_bits = explode(',', $options);
  for ($i = 0; $i < count($option_bits); $i++) {
    $option = str_replace(',', '', $option_bits[$i]);
    $option = explode(':', $option);
    $parsed_options[trim($option[0])] = trim($option[1]);
  }
  return serialize($parsed_options);
}
