<?php 

function spritely_js_spritely_js_behaviors() {
 return array(
     'spritely_js_behavior_pan' => array(
      'title' => t('Pan'),
      'description' => t('Creates a scrolling backdrop with continuous motion.'),
      'type' => 'behaviors',
      'path' => drupal_get_path('module', 'spritely_js') . '/includes/behaviors',
      'file' => 'spritely_js_behavior_pan.inc',
      'behavior' => array(
        'class' => 'spritely_js_behavior_pan',
        'parent' => 'openlayers_behavior',
      ),
    ),
    'spritely_js_behavior_sprite' => array(
      'title' => t('Sprite'),
      'description' => t('Creates an animated background image with frames per second.'),
      'type' => 'behaviors',
      'path' => drupal_get_path('module', 'spritely_js') . '/includes/behaviors',
      'file' => 'spritely_js_behavior_sprite.inc',
      'behavior' => array(
        'class' => 'spritely_js_behavior_sprite',
        'parent' => 'spritely_js_behavior_sprite',
      ),
    ),
 ); 
}