<?php
class spritely_js_behavior {
  var $instance;
  public static $_instances = array(); // not sure if we would actually need something to keep track of active instances.
  function __construct($instance) {
    $this->instance = $instance;
    self::$_instances[] = $this;
    $this->addLibrary('sites/all/libraries/spritely/jquery.spritely-0.6.js', -10);
    $this->addLibrary(drupal_get_path('module', 'spritely_js') . '/js/spritely_js.js', -9);
    $this->format_settings();
    $this->render();
  }
  function addLibrary($library, $weight = 0) {
    drupal_add_js($library, array('weight' => $weight));
  }
  function  render() {
    // Dont need anything here, but extensions should have this.
  }
  function format_settings() {
    $options = unserialize($this->instance->options);
    $options['selector'] = $this->instance->selector;
    $options['type'] = $this->instance->behavior;
    $json = array();
    $json['spritely']['spritely_' . $this->instance->iid] = $options;
    drupal_add_js($json, 'setting');
  }
  public static function instance_count() {
    return count(self::$_instances);
  }
}
?>